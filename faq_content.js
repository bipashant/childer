var questionAnswer = [
    {
        "qns": 'How do I sign up?',
        "ans": 'To sign up to childer visit <a href="www.childershop.co.nz">www.childershop.co.nz</a> and click the <a href="/en/sign_up">sign up </a> button on the top right hand side of the home page (just next to the <a href="/en/listings/new"> post a new listing </a> button). You will be directed to either sign up with your Facebook login detail or create an account with us. Once you have completed the steps as directed you will be signed up and ready to go! '
    },
    {
        "qns": 'How do I sign up as a seller?',
        "ans": 'There is no special sign up process to become a seller. You simply sign up to childer and click on the “post a new listing” button on the top right hand side of the home page to begin selling your items.</br>You will need to enter your bank account details to receive payments as your items sell. This info needs to be added to the payment section of your settings in your childer profile. To access your settings menu simply click on your profile image.'
    },
    {
        "qns": 'What are childer’s fees? ',
        "ans": 'It is free to sign up and free to list items for sale. childer charges a commission on all items sold.  We charge 11% commission on items with a sale price of $25.00 and over. For items under $25.00 there is a set transaction fee of $2.50; this is fixed to cover our minimum charges. </br> Items sold via our Depot Service are subject to 50% commission. In addition, sellers are required to pay the Stripe transaction fees.'
    },
    {
        "qns": 'How do I list an item on childer? ',
        "ans": 'First of all you will need to sign up. You can do that <a href="/en/sign_up"> here </a> Once you are logged in simply click on the <a href="/en/listings/new"> post a new listing </a> button. This can be found on the top right hand corner of the home screen on your desktop or by clicking the drop down menu on the left hand side of your mobile home screen. You will then be guided through a series of simple steps to complete your listing. '
    },
    {
        "qns": 'Who does Childer use to process its payments?',
        "ans": 'Childer uses Stripe to process all payments. Every transaction via the site is managed by Stripe. You can find out more about them <a href="https://stripe.com/nz">here</a>.'
    },
    {
        "qns": 'Are my bank account details safe on childer?',
        "ans": 'Yes. Childer uses Stripe to process all payments. Stripe has been audited by a PCI-certified auditor and is certified to PCI Service Provider Level 1. This is the most stringent level of certification available in the payments industry. To accomplish this, they make use of best-in-class security tools and practices to maintain a high level of security at Stripe. Click here (<a href="https://stripe.com/docs/security/stripe">https://stripe.com/docs/security/stripe</a>) to find out more about security at Stripe. '
    },
    {
        "qns": 'What is a routing number?',
        "ans": 'The routing number that you are asked to enter when adding your bank account is the first 6 digits of your bank account number, e.g. 065472. The remaining digits of your account number should be entered into the “account number” number box.'
    },
    {
        "qns": 'My account number is too long, why won’t it fit?',
        "ans": 'You need to enter the first six digits of your account number in the box that is titled “routing number”. Now the remaining digits should fit perfectly into the “account number box. You may need to add an additional 0 in front of the final two digits (i.e. your suffix). For example of you account suffix / final two digits are 02,  you may need to write 002.'
    },
    {
        "qns": 'Are credit card payments safe on childer? ',
        "ans": 'Yes. Childer uses Stripe to process all payments. Stripe has been audited by a PCI-certified auditor and is certified to PCI Service Provider Level 1. This is the most stringent level of certification available in the payments industry. To accomplish this, they make use of best-in-class security tools and practices to maintain a high level of security at Stripe. Click here (<a href="https://stripe.com/docs/security/stripe">https://stripe.com/docs/security/stripe</a>) to find out more about security at Stripe. '
    },
    {
        "qns": 'Why hasn’t my payment been accepted / processed?',
        "ans": 'In order for your payment to be completed the seller must accept in their childer dashboard. Sellers are not usually online 24/7 so you may have to wait a few hours for them to accept your payment. You will be notified as soon as they have approved your payment. If you are concerned about the time it is taking for a payment to be accepted you can contact the seller direct via the childer inbox or alternatively you can contact <a href="https://www.childershop.co.nz/en/user_feedbacks/new">childer</a> to see if we can assist.'
    },
    {
        "qns": 'I have accepted the buyer’s payment but it’s not in my bank account - what’s going on?',
        "ans": 'It can take up to 48 hours for your payment to be processed by your bank. If you haven’t received your payment and it has been over 48 hours please contact us at info@childer.co.nz and we will do what we can to help.'
    },
    {
        "qns": 'My item has been purchased, why is it still on the site?',
        "ans": 'Until you manually close your item it will be visible on the site. To close your listing, click on the listing item and select the option to “close listing”. This can be found on the right hand side of the screen on your desktop or at the bottom of your listing details in mobile view.'
    },
    {
        "qns": 'How should I price my item?',
        "ans": 'A good rule of thumb to follow is pricing at  25-30% of what you originally paid depending on the item’s condition. We don’t have a set price list, our only condition is that your item is not valued lower than $7.50, items priced lower than this won’t yield a worthwhile return for you or childer.'
    },
    {
        "qns": 'What if I want to change my listing once it’s been posted?',
        "ans": 'All listing can be edited at any time. You can find the edit button on the right hand side of the listing in your desk top view or at the very bottom of the listing in mobile view.'
    },
    {
        "qns": 'How can I contact a seller/ buyer? ',
        "ans": 'You can contact any seller by clicking the “contact” button on their listing page. This can be found on the right hand side of the listing page in desktop view or at the bottom of the listing info in mobile view.</br>To contact your buyer, you use your childer inbox. As soon as a buyer elects to purchase your item but clicking the buy button you are put into contact view our internal messaging service.</br>Please refer to our <a href="https://www.childershop.co.nz/en/infos/terms"> Terms of Use </a> for guidelines around communicating with other people on childer.'
    },
    {
        "qns": 'My item didn’t arrive, what should I do?',
        "ans": 'If you haven\'t received an item, you need to follow up with the seller directly. The seller should address your concerns and provide updates on the delivery of the item and its tracking information. If you find that the seller’s response is unsatisfactory you can contact childer who will contact the seller, review the issue and if appropriate suspend the sellers account.  childer will not issue any refunds on the seller’s behalf. Refunds must be paid from the seller directly. Please refer to our <a href="https://www.childershop.co.nz/en/infos/terms"> Terms of Use </a> for more information on disputes and refunds.'
    },
    {
        "qns": 'My item is not as described in the listing, what should I do? ',
        "ans": 'If there are any significant discrepancies with the item, you should contact the seller directly. The seller will address your concerns and offer a solution, such as offering a refund, a replacement or exchanging for another item. If both you and the seller have agreed on returning the item, you must return the item in the exact same condition in which it was received. It is your responsibility as the buyer to pay for the return cost of shipping. Please refer to the FAQ above for more information on how to obtain a refund or settle a dispute; alternatively you can also refer to our <a href="https://www.childershop.co.nz/en/infos/terms"> Terms of Use </a>.'
    },
    {
        "qns": 'Can I get a refund?',
        "ans": 'All sellers are able to refund the cost of an item if this is mutually agreed between both parties. childer will not issue any refunds on the seller’s behalf. Refunds must be paid from the seller directly. Please refer to our <a href="https://www.childershop.co.nz/en/infos/terms"> Terms of Use </a> for more information on disputes and refunds.'
    },
    {
        "qns": 'How do I refund a buyer?',
        "ans": 'The easiest way to refund the buyer is by bank transfer into the buyer’s nominated bank account. You would need to contact them direct to obtain this information.'
    },
    {
        "qns": 'I can’t login, what’s happening?',
        "ans": 'If you can’t login to your seller dashboard, it might be because your account has been suspended by childer. Please email us on info@childer.co.nz if you suspect this is the case.'
    },
    {
        "qns": 'There isn’t a category for my item, what should I do? ',
        "ans": 'Please contact us on info@childer.co.nz with details of the category that you would like to use and we will add your it to the listing page.'
    },
    {
        "qns": 'How do I receive my payment?',
        "ans": 'You will need to add our bank account details in the “payment” section of your “settings” panel. Once you have done this your payment will go directly into your bank account. This should happen within 2 days of you accepting the payment. Your payment will be processed using Stripe – you can find out more about the payment service <a href="https://stripe.com/nz/connect-account/legal#translation">here</a>.'
    },
    {
        "qns": 'What items / actions aren’t allowed on childer? ',
        "ans": 'Below are details of items / actions prohibited on childer. By using our site you agree that you will not: <ul>' +
        '<li>sell or list unwashed items.</li>' +
        '<li>sell or list items that are damaged. </li>' +
        '<li>sell or list items that do not comply with our <a href="#">Seller Guidelines</a></li>' +
        '<li>sell or list any item that is illegal to sell under any applicable law, statute, ordinance, or regulation, including but not limited to replica, counterfeit, or stolen items; child pornography; obscene materials; drugs; trade secrets; or items that have been identified by any New Zealand authority as hazardous to consumers and therefore subject to recall;</li>' +
        '<li>impersonate any person or entity; </li>' +
        '<li>stalk, threaten, or otherwise harass any person, or carry any weapons; </li>' +
        '<li>violate any law, statute, rule, permit, ordinance or regulation; </li>' +
        '<li>interfere with or disrupt the Services;</li>' +
        '<li>post information through or interact with, the Services in a manner which is false, inaccurate, misleading (directly or by omission or failure to update information), defamatory, libelous, abusive, obscene, profane, offensive, sexually orientated, threatening, harassing or illegal;</li>' +
        '<li>use the Services in any way that infringes any third party’s rights, including but not limited to: intellectual property rights, copyright, patent, trademark, trade secret or other proprietary rights or rights of publicity or privacy;</li>' +
        '<li>post, email or otherwise transmit any malicious code, files or programs designed to interrupt, damage, destroy or limit the functionality of any computer software or hardware or telecommunications equipment or surreptitiously intercept or expropriate any system data or personal information.</li>' +
        '<li>"frame" or "mirror" any part of the Services, without our prior written authorization or use meta tags or code or other devices containing any reference to us in order to direct any person to any other website for any purpose; o</li>' +
        '<li>modify, adapt, translate, reverse engineer, decipher, decompile or otherwise disassemble any portion of the Services or any software used on or for the Services;</li>' +
        '<li>rent, lease, lend, sell, redistribute, license, sub-license or access to any portion of the Services; </li>' +
        '<li>use any robot, spider, site search/retrieval application, or other manual or automatic device or process to retrieve, index, scrape, "data mine", or in any way reproduce or circumvent the navigational structure or presentation of the Services or its contents; </li>' +
        '<li>link directly or indirectly to any other websites; </li>' +
        '<li>transfer or sell your account, password and / or identification to any other party; </li>' +
        '<li>discriminate against or harass anyone on the basis of race, national origin religion, gender, gender identity, physical or mental disability, medical condition, marital status, age or sexual orientation, or</li>' +
        '<li>arrange the swap or trade of items with other members on childer;</li>' +
        '<li>cause any third party to engage in the restricted activities above.</li></ul>'

    },
    {
        "qns": 'Why has my item been deleted?',
        "ans": 'We strive to maintain consistently good quality items for the mums and dads using the site. If we feel that your item does not meet the required level of quality we will delete it from the site and contact you to let you know. Please check out our <a href="#">Seller Guidelines</a>  for more information on what we will and won’t accept on childer.'
    },
    {
        "qns": 'I forgot my password. How do I reset it?',
        "ans": 'Please visit the sign in page and select the “forgot username or password” option under the sign in form. You will be guided through the steps to create a new password from there.'
    },
    {
        "qns": 'Do you allow guest checkout?',
        "ans": 'No. You must sign up to childer to purchase items from the site.'
    }

];
