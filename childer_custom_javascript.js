
$(document).ready(function () {
    if ($("#homepage-filters").length > 0) {
        console.log("homepage detected");
        if ($(".marketplace-lander-content-title").length > 0) {
            //console.log("user is not logged In");
        }
        else {
            console.log("user is logged In");
            $(".marketplace-lander").append('<div class="coverimage"><figure class="marketplace-cover fluidratio"> <div class="lander-content marketplace-lander-content"> <h1 class="marketplace-lander-content-title">. </h1> <p class="marketplace-lander-content-description">Buy & sell preloved children\'s clothing and accessories</p> </div> </figure> </div>');
        }
    } else {
        console.log("Not Homepage");
        $(".title-container").css("background", "#fff").css("border-bottom", "1px solid rgba(0,0,50,0.1)");
    }

    $("a:contains('How it works')").find(".left-navi-link-text").text("Depot Service");
    $("div.text-with-icon:contains('How it works')").find(".left-navi-link-text").text("Depot Service");
    $("div.text-with-icon:contains('How it works')").text("Depot Service")
    $("div.text-with-icon:contains('Privacy')").text("Privacy Policy");
    $('*[data-pid="Depot Service 0"]').removeAttr("target");
    $('*[data-reactid="29"]').removeAttr("target");

    setTimeout(function () {
        $('a[href="/invitations/new"]').text("FAQs").attr("href", "#faq-popup").addClass("faq-popup-trigger");
        $('a[href="/en/invitations/new"]').text("FAQs").attr("href", "#faq-popup").addClass("faq-popup-trigger");
        $(".faq-popup-trigger").click(function () {
            $("body").addClass("faq-open");
        });
    }, 1);
    $('body').append('<footer><div class=layout-centered-content><div class="row footer-links"><div class="col-xs-12 col-sm-4"><div class=row><h3>Top categories</h3><div class="col-sm-12 col-xs-6"><a href="/?category=babies-0-2">Babies 0-2</a></div><div class="col-sm-12 col-xs-6"><a href="/?category=kids-2-10">Kids 2-10</a></div><div class="col-sm-12 col-xs-6"><a href="/?category=accessories">Accessories</a></div><div class="col-sm-12 col-xs-6"><a href="/?category=toys-and-books">Toys & Books</a></div></div></div><div class="col-xs-12 col-sm-4"><div class=row><h3>Learn more</h3><div class="col-sm-12 col-xs-6"><a href=/en/infos/about>About us</a></div><div class="col-sm-12 col-xs-6"><a href=/en/infos/how_to_use>Depot service</a></div><div class="col-sm-12 col-xs-6"><a href=/en/infos/privacy>Privacy policy</a></div><div class="col-sm-12 col-xs-6"><a href=/en/infos/terms>Terms of use</a></div><div class="col-sm-12 col-xs-6"><a class= "faq-popup-trigger" href=#faq-popup>FAQs</a></div></div></div><div class="col-xs-12 col-sm-4"><div class=row><h3>Talk to us</h3><div class="col-sm-12 col-xs-6"><a href=/en/user_feedbacks/new>Send us a message</a></div><div class=col-xs-12><ul class=social-links-list><li class=instagram><a href=https://instagram.com/childer_nz/ class="fa fa-instagram"></a><li class=facebook><a href=https://www.facebook.com/childernz class="fa fa-facebook"></a></ul></div></div></div></div><div class="row footer-link text-center" style="font-size: 14px;color: #959494;">All rights reserved �2017 Childer</div></div></footer>');


    // $('body').append('');

    var popUpcontent = '<div id="faq-popup" class="faq-overlay"><div class="faq-popup"><div class="popup-header"><h2>Frequently Asked Questions</h2><a class="close faq-popup-close" href="#">&times;</a><div class="faq-searchbar-wrapper"><div class="faq-search-input-wrapper"><input type="search" class="faq-searchbar" placeholder="Search..."><button type="submit" class="SearchBar__searchButton__1Ck2b" style="background-color:transparent;" data-reactid="54"><svg width="17" height="17" viewBox="336 14 17 17" xmlns="http://www.w3.org/2000/svg"><g opacity=".7" fill="none" fill-rule="evenodd" transform="matrix(-1 0 0 1 352 15)" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"><path d="M11 11l3.494 3.494"></path><circle cx="6" cy="6" r="6"></circle></g></svg></button></div></div></div><div class="main-content"><div class="box"><ul class="question-list">';
    var qnsList = $.map(questionAnswer, function (value, index) {
        // debugger;
        return ('<li id="link-for-question-' + index + '"><a href=#question-' + index + ' class="question-link">' + value.qns + '</a></li>');
    });
    var qnsListString = '';
    $.each(qnsList, function () {
        qnsListString += this || '';
    });

    var popUpcontent1 = '</ul></div><div class="content">';

    var qnsAnsList = $.map(questionAnswer, function (value, index) {
        return ('<div class="box" id="question-' + index + '">' + '\n<div class="question">' + value.qns + '</div>\n<div class="answer">' + value.ans + '</div>\n<div class="top-link"><span class="fa fa-arrow-up navigate-top">TOP</span>\n</div>\n</div>');
    });
    var qnsAnsListString = '';
    $.each(qnsAnsList, function () {
        qnsAnsListString += this || '';
    });

    var popUpcontent2 = '</div></div></div></div>';
    $('body').append(popUpcontent + qnsListString + popUpcontent1 + qnsAnsListString + popUpcontent2);

    $(".faq-popup-close").click(function () {
        $("body").removeClass("faq-open");
    });

    $(".question-link").click(function (e) {
        e.preventDefault();
        $('#faq-popup .main-content').animate({
            scrollTop: $($(this).attr("href")).offset().top - 200
        }, 500);
    });

    $(".navigate-top").click(function (e) {
        e.preventDefault();
        $('#faq-popup .main-content').animate({
            scrollTop: 0
        }, 500);
    });

    // check if #faq-popup present in url while page reload
    var url = window.location.href;
    // Get DIV
    // Check if URL contains the keyword
    if (url.search('#faq-popup') > 0) {
        // Display the message
        // $("body").addClass("faq-open");
        setTimeout(function () { $(".faq-popup-trigger")[0].click();}, 500);
    }


    // Search question and answer based on user input on faq search textbox
    $(".faq-searchbar").on('input propertychange paste', function () {
        var searchKey = $(this).val();
        $.each(questionAnswer, function (index, value) {
            if (value.qns.toLowerCase().includes(searchKey.toLowerCase()) || value.ans.toLowerCase().includes(searchKey.toLowerCase())) {
                $("#link-for-question-" + index).show();
                $("#question-" + index).show();
            }
            else {
                $("#link-for-question-" + index).hide();
                $("#question-" + index).hide();
            }

        });
        $('#faq-popup .main-content').animate({
            scrollTop: 0
        }, 500);
    });

});
